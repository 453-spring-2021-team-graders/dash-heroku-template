# Deploy Dash App on Heroku

This example dash app is deployed in this [link](https://c00269380-dashapp.herokuapp.com/).

## Steps to develop the app locally

1. Clone this repository:

```bash
git clone https://gitlab.com/453-spring-2021-team-graders/dash-heroku-template.git
```

2. Navigate to the `dash-heroku-template` folder using the `cd` command.

```bash
cd dash-heroku-template
```

3. Create a virtualenv

```bash
virtualenv .venv
```

4. Activate the virtualenv:

```bash
source .venv/bin/activate       # for UNIX
```

```bash
source .venv/Scripts/activate   # for Windows bash
```

```cmd
.venv\Scripts\activate.bat      # for Windows CMD
```

5. Install Python requirements in the environment.

```bash
pip install -r requirements.txt
```

6. Run the app

```bash
python app.py
```

Open the [link](http://127.0.0.1:5000) in the browser displayed in the log when you follow step 6.

```bash
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
```

## Steps to deploy the app on Heroku

1. [Setup an account](https://signup.heroku.com/) on Heroku and [download the Heroku CLI utility](https://devcenter.heroku.com/articles/heroku-cli#install-the-heroku-cli).

2. Log in to Heroku from CLI:

```bash
heroku login
```

At this point, the default browser will prompt you to authorize the bash to use the Heroku CLI.

3. Create a Heroku app from CLI:

**NOTE**: Skip this step if you have already created `YOUR-APP-NAME` on Heroku via the UI (browser).

```bash
heroku create -n YOUR-APP-NAME
```

Where `YOUR-APP-NAME` refers to the title of your Dash app.

For example, if your UILD is `c00269420`, then `YOUR-APP-NAME` would be `c00269420-firstapp`.

4. Link the git repository on your machine with the git on Heroku:

```bash
heroku git:remote -a YOUR-APP-NAME
```

5. Add all the files in this directory:

```bash
git add .
```

6. Create a commit message:

```bash
git commit -am "First push to Heroku git"
```

7. Deploy the app to Heroku:

```bash
git push heroku master
```

Heroku deploys your app at: `https://YOUR-APP-NAME.herokuapp.com`.

To make changes to the app, repeat steps 5, 6, and 7.

## Credits

-   Heroku Dash template repository: [dash-heroku-template](https://github.com/plotly/dash-heroku-template).
